﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public abstract class TomatoHero3DBehaviour : MonoBehaviour
{
    #region Private Variables

    private bool hasRegistered = false;

    protected GameManager gameManager;

    protected bool IsInputSystemInterrupted { get { return BaseInputSystem.IsInputSystemInterrupted; } }

    #endregion

    #region Public Callback



    public void Register()
    {
        if (!hasRegistered)
        {

            gameManager = GameManager.Instance;

            gameManager.gameStateController.OnDataLoaded += OnDataLoaded;
            gameManager.gameStateController.OnSceneLoaded += OnSceneLoaded;
            gameManager.gameStateController.OnLevelStarted += OnLevelStarted;
            gameManager.gameStateController.OnLevelEnded += OnLevelEnded;
            gameManager.gameStateController.OnStateChanged += OnStateChanged;



            hasRegistered = true;
        }
    }

    public void Unregister()
    {

        if (hasRegistered)
        {
            gameManager.gameStateController.OnDataLoaded -= OnDataLoaded;
            gameManager.gameStateController.OnSceneLoaded -= OnSceneLoaded;
            gameManager.gameStateController.OnLevelStarted -= OnLevelStarted;
            gameManager.gameStateController.OnLevelEnded -= OnLevelEnded;
            gameManager.gameStateController.OnStateChanged -= OnStateChanged;


            hasRegistered = false;
        }
    }

    #endregion

    #region Configuretion


    private IEnumerator ControllerForWaitingUntilInterruptionRevoked(UnityAction OnInterruptionRevoked)
    {
        WaitUntil _waitUntil = new WaitUntil(() =>
        {
            if (BaseInputSystem.IsInputSystemInterrupted)
                return false;

            return true;
        });

        yield return _waitUntil;
        OnInterruptionRevoked?.Invoke();
        StopCoroutine(ControllerForWaitingUntilInterruptionRevoked(null));
    }

    #endregion

    #region MonoBehaviour

    protected virtual void Awake()
    {


    }

    protected virtual void OnEnable()
    {
        Register();
    }

    protected virtual void OnDisable()
    {
        Unregister();
    }

    protected virtual void OnDestroy()
    {

        Unregister();
    }

    #endregion

    #region VirtualFunction

    public virtual void OnDataLoaded()
    {


    }

    public virtual void OnSceneLoaded()
    {


    }

    public virtual void OnLevelStarted()
    {


    }

    public virtual void OnLevelEnded()
    {


    }

    public virtual void OnStateChanged(GameState gameState)
    {


    }

    #endregion

    #region Protected callback

    protected void WaitUntilInterruptionRevoked(UnityAction OnInterruptionRevoked)
    {
        StartCoroutine(ControllerForWaitingUntilInterruptionRevoked(OnInterruptionRevoked));
    }

    protected void SetStatusFlagForInputSystem(bool isInterrupted)
    {
        Debug.Log("InterruptionStatus : " + isInterrupted);
        BaseInputSystem.IsInputSystemInterrupted = isInterrupted;
    }

    protected void UIAppear(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIAppear.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    protected void UIDisappear(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIDisappear.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    protected void UIInteract(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIInteract.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    #endregion
}
