﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPreviewVertex : MonoBehaviour
{
    public RawImage rawImageReference;
    public DecaleCharacterObjectUsingVertex decaleObjectReference;


    // Update is called once per frame
    void Update()
    {
        rawImageReference.texture = decaleObjectReference.meshRenderer.material.mainTexture;
    }
}
