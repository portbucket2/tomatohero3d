﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using com.faith.core;

public class WeaponController : MonoBehaviour
{
    #region Public Variables

    [Space(5.0f)]
    public WeaponAttributeAsset weaponAttribute;

    [Space(5.0f)]
    public GameObject fbxContainer;
    public Transform muzzlePosition;


    #endregion

    #region Private Variables

    private bool _isAllowedToShoot = true;

    private int _weaponID;
    private int _remainingAmmunitionInMagazine;
    private int _remainingAmmunition;
    private int _bulletIndex;

    private ParticleSystem _particleEffectsForMuzzleFlash;

    private WaitForSeconds _delayAsRateOfFire;

    private UnityAction<int> OnPassingRemainingAmmunitionInMagazine;
    private UnityAction<int> OnPassingRemainingAmmunition;
    private UnityAction<int,float> OnReloading;

    #endregion

    #region Mono Behaviour

    public void Awake()
    {
        _remainingAmmunitionInMagazine   = weaponAttribute.clipSize;
        _remainingAmmunition             = weaponAttribute.maxAmmoSize;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForReloading(UnityAction OnReloadEnd = null) {

        float remainingTimeForReloading = weaponAttribute.durationForReloading;
        float cycleLength = 0.0167f;
        WaitForSeconds cycleDelay = new WaitForSeconds(0.0167f);

        while (remainingTimeForReloading > 0)
        {

            remainingTimeForReloading -= cycleLength;

            OnReloading?.Invoke(_weaponID,1f - (remainingTimeForReloading / weaponAttribute.durationForReloading));

            yield return cycleDelay;
        }

        OnReloading?.Invoke(_weaponID,1);

        _remainingAmmunitionInMagazine = Mathf.Clamp(weaponAttribute.clipSize, 0, (_remainingAmmunition < weaponAttribute.clipSize) ? _remainingAmmunition : weaponAttribute.clipSize);
        _remainingAmmunition -= _remainingAmmunitionInMagazine;

        if (weaponAttribute.isInfiniteAmmo)
            _remainingAmmunition = weaponAttribute.maxAmmoSize;

        OnPassingRemainingAmmunitionInMagazine?.Invoke(_remainingAmmunitionInMagazine);
        OnPassingRemainingAmmunition?.Invoke(_remainingAmmunition);

        _isAllowedToShoot = true;

        OnReloadEnd?.Invoke();
    }

    private IEnumerator ShootingDelayBasedOnRateOfFire() {

        _isAllowedToShoot = false;

        yield return _delayAsRateOfFire;

        if (_remainingAmmunitionInMagazine > 0)
            _isAllowedToShoot = true;
        else if (_remainingAmmunition > 0) {

            yield return ControllerForReloading();
        }

        StopCoroutine(ShootingDelayBasedOnRateOfFire());
    }

    #endregion

    #region Public Callback

    public void InitializeWeapon(
        int weaponID = -1,
        UnityAction<int> OnPassingRemainingAmmunitionInMagazine = null,
        UnityAction<int> OnPassingRemainingAmmunition = null,
        UnityAction<int,float> OnReloading = null) {

        _bulletIndex = 0;
        _delayAsRateOfFire = new WaitForSeconds(weaponAttribute.RateOfFire);

        if (_particleEffectsForMuzzleFlash != null)
            Destroy(_particleEffectsForMuzzleFlash.gameObject);

        if(weaponAttribute.muzzleFlash != null)
            _particleEffectsForMuzzleFlash = Instantiate(weaponAttribute.muzzleFlash, muzzlePosition).GetComponent<ParticleSystem>();

        _weaponID = weaponID;
        this.OnPassingRemainingAmmunitionInMagazine = OnPassingRemainingAmmunitionInMagazine;
        this.OnPassingRemainingAmmunition = OnPassingRemainingAmmunition;
        this.OnReloading = OnReloading;

        OnPassingRemainingAmmunitionInMagazine?.Invoke(_remainingAmmunitionInMagazine);
        OnPassingRemainingAmmunition?.Invoke(_remainingAmmunition);
    }

    public bool ShootTracerBullet(Transform target, UnityAction OnBulletReached, params TagReference[] ignoreCollisionWithTags)
    {
        if (_isAllowedToShoot)
        {

            BulletController bulletController = Pool.GlobalPoolManager.Instantiate(
                weaponAttribute.bulletParameters.bulletPrefab,
                muzzlePosition.position,
                Quaternion.identity).GetComponent<BulletController>();

            bulletController.name = string.Format("{0} <-> {1}", weaponAttribute.bulletParameters.bulletPrefab.name, _bulletIndex++);
            bulletController.transform.position = muzzlePosition.position;

            bulletController.ShootOnTarget(
                    weaponAttribute.bulletParameters.damagePerShoot,
                    weaponAttribute.bulletParameters,
                    target,
                    weaponAttribute.bulletParameters.travelingForwardVelocity,
                    OnBulletReached,
                    ignoreCollisionWithTags
                );

            //bulletController.transform.SetParent(null);

            _remainingAmmunitionInMagazine--;
            OnPassingRemainingAmmunitionInMagazine?.Invoke(_remainingAmmunitionInMagazine);

            StartCoroutine(ShootingDelayBasedOnRateOfFire());

            if (_particleEffectsForMuzzleFlash != null && !_particleEffectsForMuzzleFlash.isPlaying)
                _particleEffectsForMuzzleFlash.Play();

            return true;
        }

        return false;
    }

    public bool Shoot(Vector3 targetDirection, params TagReference[] ignoreCollisionWithTags) {

        if (_isAllowedToShoot) {

            BulletController bulletController = Pool.GlobalPoolManager.Instantiate(
                weaponAttribute.bulletParameters.bulletPrefab,
                muzzlePosition.position,
                Quaternion.identity).GetComponent<BulletController>();
            bulletController.name = string.Format("{0} <-> {1}", weaponAttribute.bulletParameters.bulletPrefab.name, _bulletIndex++);

            bulletController.ShootOnDirection(
                    weaponAttribute.bulletParameters.damagePerShoot,
                    weaponAttribute.bulletParameters,
                    targetDirection,
                    weaponAttribute.bulletParameters.travelingForceMode,
                    weaponAttribute.bulletParameters.travelingForwardVelocity,
                    ignoreCollisionWithTags
                );


            _remainingAmmunitionInMagazine--;
            OnPassingRemainingAmmunitionInMagazine?.Invoke(_remainingAmmunitionInMagazine);

            StartCoroutine(ShootingDelayBasedOnRateOfFire());

            if(_particleEffectsForMuzzleFlash != null && !_particleEffectsForMuzzleFlash.isPlaying)
                _particleEffectsForMuzzleFlash.Play();

            return true;
        }

        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="OnReloadEnd"></param>
    /// <returns>true : if there are any ammunition available to fire</returns>
    public bool Reload(UnityAction OnReloadEnd = null) {

        if (_remainingAmmunition > 0)
        {
            ControllerForReloading(OnReloadEnd);
            return true;
        }

        return false;
    }

    #endregion
}
