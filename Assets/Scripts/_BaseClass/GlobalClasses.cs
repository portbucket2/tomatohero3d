﻿
[System.Serializable]
public class SharedData
{
    public int levelIndex = 0;

    public int numberOfWaveInThisLevel = 0;
    public int remainingNumberOfWaveInThisLevel = 0;

    public int numberOfEnemyToDefeateOnCurrentWave = 0;
    public int remainingNumberOfEnemyToDefeateOnCurrentWave = 0;
}

public class Constant
{
    public const int EXECUTION_ORDER_GAMEMANAGER = -1000;
    public const int EXECUTION_ORDER_STATEMACHINE = -900;
    public const int EXECUTION_ORDER_RAILSHOOTERBEHAVIOUR = -100;
}