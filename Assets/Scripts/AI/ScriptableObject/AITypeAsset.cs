﻿using UnityEngine;

[CreateAssetMenu(fileName = "AITypeAsset", menuName = GlobalConstant.GAME_NAME + "/AI/AITypeAsset")]
public class AITypeAsset : BaseEnumAsset
{
    #region Public Property

    public AIBehaviourAsset Behaviour { get { return behaviour; } }
    public GameObject BluePrint { get { return bluePrint; } }

    #endregion

    #region SerializedField

    [Header("Parameter  :   Base")]
    [SerializeField] private AIBehaviourAsset behaviour;
    [SerializeField] private GameObject       bluePrint;

    #endregion

}
