﻿public enum GameState
{
    None,
    DataLoaded,
    SceneLoaded,
    LevelStarted,
    LevelEnded
}
