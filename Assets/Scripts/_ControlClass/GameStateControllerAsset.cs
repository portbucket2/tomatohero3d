﻿using UnityEngine;
using System;
using System.Collections.Generic;
using com.faith.core;
[CreateAssetMenu(fileName = "GameStateControllerAsset", menuName = GlobalConstant.GAME_NAME + "/GameFlow/GameStateControllerAsset", order = 1)]
[DefaultExecutionOrder(Constant.EXECUTION_ORDER_STATEMACHINE)]
public class GameStateControllerAsset : ScriptableObject
{
    #region Public Variables

    public GameState gameState
    { get; private set; }

    public event Action OnDataLoaded;
    public event Action OnSceneLoaded;
    public event Action OnLevelStarted;
    public event Action OnLevelEnded;

    public event Action<GameState> OnStateChanged;



    #endregion

    #region Private Variables

    [SerializeField] private int _stackSize = 32;
    [SerializeField] private List<GameState> _stackForGameState = new List<GameState>();

    #endregion

    #region ScriptableObject

    public void OnEnable()
    {
        _stackForGameState = new List<GameState>();
    }

    #endregion

    #region Configuretion

    private bool IsValidStackIndex(int stackIndex)
    {
        return (stackIndex >= 0 && stackIndex < _stackSize) ? true : false;
    }

    private void ManageStackForGameState(GameState gameState)
    {

        int stackCount = _stackForGameState.Count;
        if (stackCount > 0)
        {
            if (_stackForGameState[stackCount - 1] != gameState)
                _stackForGameState.Add(gameState);
        }
        else
        {
            _stackForGameState.Add(gameState);
        }

        if (stackCount >= _stackSize)
        {
            Debug.LogWarning("StackOverFlow : GameState. Removing(0) : " + _stackForGameState[0] + ", gameState from the stack");
            _stackForGameState.RemoveAt(0);
        }
    }

    #endregion

    #region Public Callback

    public void ChangeGameState(GameState gameState)
    {

        ManageStackForGameState(gameState);

        this.gameState = gameState;

        Debug.Log("GameStateChanged : " + gameState);
        OnStateChanged?.Invoke(gameState);

        switch (gameState)
        {

            case GameState.SceneLoaded:
                OnSceneLoaded?.Invoke();
                break;

            case GameState.DataLoaded:
                OnDataLoaded?.Invoke();
                break;

            case GameState.LevelStarted:
                OnLevelStarted?.Invoke();
                break;

            case GameState.LevelEnded:
                OnLevelEnded?.Invoke();
                break;

        }
    }

    public GameState GetPreviousStateFromStack()
    {

        return GetStateFromStack(_stackForGameState.Count - 1);
    }

    public GameState GetStateFromStack(int stackIndex = -1)
    {

        if (IsValidStackIndex(stackIndex))
            return _stackForGameState[stackIndex];
        else
        {

            Debug.LogError("Invalid stackIndex = " + stackIndex + ", must be withing [0," + _stackSize + "]. returning 'None' State");
            return GameState.None;
        }
    }

    #endregion
}
