﻿using UnityEngine;
using com.faith.core;
[CreateAssetMenu(fileName = "PhysicsImpact", menuName = GlobalConstant.GAME_NAME + "/PhysicsImpact")]
public class PhysicsImpactAsset : ScriptableObject
{
    #region Public Varaibles

    [SerializeField] private ForceMode forceMode;
    [SerializeField] private RangeReference force;

    #endregion

    #region Public Callback

    public void ExecutePhysicsImpact(ref Rigidbody rigidbodyReference, Vector3 direction) {

        rigidbodyReference.AddForce(
                force * direction,
                forceMode
            );
    }

    #endregion

}
