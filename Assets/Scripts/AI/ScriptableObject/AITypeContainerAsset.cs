﻿using UnityEngine;

[CreateAssetMenu(fileName = "AITypeContainerAsset", menuName = GlobalConstant.GAME_NAME + "/AI/AITypeContainerAsset")]
public class AITypeContainerAsset : ScriptableObject
{
    #region Public Property

    public GameObject GetRandomAIBlueprint
    {
        get {
            return aIAssetTypes[Random.Range(0, aIAssetTypes.Length)].BluePrint;
        }
    }

    #endregion

    #region SerializedField

    [SerializeField] private AITypeAsset[] aIAssetTypes;

    #endregion
}
