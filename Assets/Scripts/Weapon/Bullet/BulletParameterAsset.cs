﻿using UnityEngine;
using com.faith.core;

[CreateAssetMenu(fileName = "BulletParameterAsset", menuName = GlobalConstant.GAME_NAME + "/WeaponSystem/BulletParameterAsset", order = 21)]
public class BulletParameterAsset : ScriptableObject
{
    #region Public Properties

    public GameObject GetRandomPrefabReferenceForImpactParticle
    {
        get
        {
            return particlesForImpact[Random.Range(0, particlesForImpact.Length)];
        }
    }

    #endregion

    #region Public Variables

    public GameObject       bulletPrefab;
    public FloatReference   damagePerShoot;
    public FloatReference   lifeTime;
    public TagReference[]   ignoringObjectsWithTags;

    [Header("Parameter  :   Movement")]
    public ForceMode        travelingForceMode;
    public float            travelingForwardVelocity;

    [Header("Parameter  :   ParticleImapct")]
    public bool isParticleImpactEnabled = false;
    public GameObject[] particlesForImpact;

    [Header("Parameter  :   PhysicsImpact")]
    public bool             isPhysicsImpactEnabled = false;

    [Space(2.5f)]
    public FloatReference   force;
    public RangeReference   impactRadius;

    [Space(2.5f)]
    public ForceMode        forceMode;

    [Space(2.5f)]
    public LayerMask        impactLayer;

    #endregion

    

}
