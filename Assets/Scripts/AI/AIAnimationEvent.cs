﻿
using UnityEngine;

public class AIAnimationEvent : MonoBehaviour
{
    public AIController aiControllerReference;

    public void ThrowTomato() {

        aiControllerReference.ThrowTomato();
    }

}
