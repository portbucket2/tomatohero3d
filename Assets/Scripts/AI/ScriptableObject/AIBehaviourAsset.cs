﻿using UnityEngine;
using com.faith.core;

[CreateAssetMenu(fileName = "AIBehaviourAsset", menuName = GlobalConstant.GAME_NAME + "/AI/AIBehaviourAsset")]
public class AIBehaviourAsset : ScriptableObject
{
    #region Public Access

    public RangeReference       DurationForStayingIdle { get { return durationForStayingIdle; } }
    public float       ProbabilityToShoot { get { return probabilityToShoot; } }

    public PhysicsImpactAsset   PhysicsImpact { get { return physicsImpact; } }
    public GameObject           GetBlueprintForOnDeathParticle { get { return onDeathParticles[Random.Range(0, onDeathParticles.Length)]; } }

    #endregion

    #region SerializedField

    [Header("Parameter  :   Base")]

    [Space(5.0f)]
    [SerializeField] private  RangeReference   durationForStayingIdle;
    [SerializeField, Range(0f,1f)] private  float   probabilityToShoot = 0.1f;


    [Header("Parameter  :   OnDeath")]
    [SerializeField] private PhysicsImpactAsset physicsImpact;
    [SerializeField] private GameObject[]     onDeathParticles;

    #endregion


}
