﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class UIGameplayMenu : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        nextButton.onClick.AddListener(() =>
        {
            gameManager.LoadScene(
                gameManager.levelData.levelInformations[gameManager.SharedGameData.levelIndex].SceneReference,
                OnSceneLoaded : ()=> {
                    gameManager.ChangeGameState(GameState.LevelStarted);
                });
        });
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnLevelStarted()
    {
        base.OnLevelStarted();

        StartCoroutine(OnLevelStartWithDelay());
    }

    public override void OnLevelEnded()
    {
        base.OnLevelEnded();

        gameManager.characterControllerForPlayer.OnDamageTaken -= UpdatePlayerHealth;

        int levelIndex              = gameManager.SharedGameData.levelIndex + 1;
        float progression           = 1f - (gameManager.SharedGameData.remainingNumberOfEnemyToDefeateOnCurrentWave / ((float)gameManager.SharedGameData.numberOfEnemyToDefeateOnCurrentWave));

        levelText.text              = string.Format("LEVEL {0}", levelIndex < 10 ? "0" + levelIndex : levelIndex.ToString());
        if (progression < 1)
        {
            gameOverText.text       = "Oh No!";
            celebretionText.text    = "Retry";
        }
        else {

            gameOverText.text       = "Congratulation";
            celebretionText.text    = "Continue";
            gameManager.levelData.GotToNextLevel();
        }

        UIAppear(popupPanelForGameOver);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public GameObject rootContainer;


    [Header("Reference  :   PlayerHUD")]
    public Image UIIHealth;

    [Header("Reference  :   Progress")]
    public TextMeshProUGUI UITCurrentLevelIndicator;
    public TextMeshProUGUI UITProgressionIndicator;
    public Image UIIProgressbar;

    [Header("Reference  :   GameOverPanel")]
    public Transform popupPanelForGameOver;
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI celebretionText;
    public TextMeshProUGUI nextButtonText;
    public Button nextButton;

    #endregion

    #region Private Variables

    private float _playerHealth;

    #endregion

    #region Configuretion

    private IEnumerator OnLevelStartWithDelay() {

        yield return new WaitForSeconds(0.1f);

        _playerHealth = gameManager.characterControllerForPlayer.health;
        gameManager.characterControllerForPlayer.OnDamageTaken += UpdatePlayerHealth;

        int levelIndex = gameManager.levelData.GetLevelIndex + 1;
        UITCurrentLevelIndicator.text = string.Format("LEVEL {0}", levelIndex < 10 ? "0" + levelIndex : levelIndex.ToString());

        UpdatePlayerHealth(_playerHealth);
        UpdatePlayerProgression();

        rootContainer.SetActive(true);

        StopCoroutine(OnLevelStartWithDelay());
    }

    #endregion

    #region Public Callback

    public void UpdatePlayerHealth(float currentHealth) {

        UIIHealth.fillAmount = currentHealth / _playerHealth;
    }

    public void UpdatePlayerProgression() {

        int waveIndex = gameManager.SharedGameData.numberOfWaveInThisLevel - gameManager.SharedGameData.remainingNumberOfWaveInThisLevel;
        waveIndex = waveIndex == 0 ? 1 : waveIndex;
        UITProgressionIndicator.text = string.Format("WAVE {0}", waveIndex < 10 ? "0" + waveIndex : waveIndex.ToString());

        float progression = 1f - (gameManager.SharedGameData.remainingNumberOfEnemyToDefeateOnCurrentWave / ((float)gameManager.SharedGameData.numberOfEnemyToDefeateOnCurrentWave));
        UIIProgressbar.fillAmount = progression;

    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
