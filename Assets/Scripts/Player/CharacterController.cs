﻿
using UnityEngine;
using com.faith.core;

public class CharacterController : TomatoHero3DBehaviour, IDamage
{
    #region ALL UNITY FUNCTIONS

    protected override void Awake()
    {
        base.Awake();

        ResetHealth();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public event System.Action<float>               OnDamageTaken;
    public event System.Action<CharacterController> OnDied;

    public FloatReference       health;
    public WeaponController     selectedWeapon;
    public Collider             colliderReference;

    public bool IsAlive { get; private set; }

    #endregion

    #region Private Variables

    public float _currentHealth { get; private set; }

    #endregion

    #region Public Callback

    public void ResetHealth() {

        colliderReference.enabled = true;
        _currentHealth = health;
        IsAlive = true;
    }

    public void Damage(float damageValue)
    {

        _currentHealth -= damageValue;
        _currentHealth = Mathf.Clamp(_currentHealth, 0, Mathf.Infinity);

        OnDamageTaken?.Invoke(_currentHealth);

        if (_currentHealth <= 0)
        {
            if (IsAlive) {

                IsAlive = false;
                colliderReference.enabled = false;
                OnDied?.Invoke(this);
            }
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
