﻿using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NavMeshAgentControllerAsset", menuName = "TomatoHero3D/NavMesh/NavMeshAgentControllerAsset")]
public class NavMeshAgentControllerAsset : ScriptableObject
{

    #region Custom Variables

    public class NavMeshAgentMovementController
    {

        #region Public Variables

        public bool IsControllerRunning { get; private set; }

        public NavMeshAgent ReferenceOfNavMeshAgent { get; private set; }

        #endregion

        #region Private Variables

        private CancellationTokenSource _canceletionToken;
        private bool _isForceQuit;
        private int _currentIndexOfWaypoint;
        private int _numberOfWaypoints;
        private float _validDistanceFromDestination;
        private Transform _transformReferenceOfNavMeshAgent;
        private Transform[] _waypoints;
        private UnityAction _OnReachingDestination;



        #endregion

        #region Configuretion

        private void CalculateValidDistance() {

            if (_currentIndexOfWaypoint == (_numberOfWaypoints - 1))
                _validDistanceFromDestination = 0.1f;
            else
            {
                _validDistanceFromDestination = Vector3.Distance(_transformReferenceOfNavMeshAgent.position, _waypoints[_currentIndexOfWaypoint].position) * 0.1f;
            }
        }

        private bool AssignNewWaypointIfValid(int newCurrentIndexOfWaypoint)
        {

            if (newCurrentIndexOfWaypoint >= 0 && newCurrentIndexOfWaypoint < _numberOfWaypoints)
            {
                _currentIndexOfWaypoint = newCurrentIndexOfWaypoint;
                CalculateValidDistance();
                

                return true;
            }

            return false;
        }

        private void UpdateNewDestination(int newCurrentIndexOfWaypoint)
        {

            if (AssignNewWaypointIfValid(newCurrentIndexOfWaypoint))
            {
                ReferenceOfNavMeshAgent.SetDestination(_waypoints[_currentIndexOfWaypoint].position);
            }
        }

        #endregion

        #region Public Callback

        public NavMeshAgentMovementController(NavMeshAgent navMeshAgentReference)
        {

            _canceletionToken = new CancellationTokenSource();

            _isForceQuit = false;
            ReferenceOfNavMeshAgent = navMeshAgentReference;
            _transformReferenceOfNavMeshAgent = ReferenceOfNavMeshAgent.transform;

            ReferenceOfNavMeshAgent.enabled = false;
            NavMeshHit closestHit;
            if (NavMesh.SamplePosition(_transformReferenceOfNavMeshAgent.position, out closestHit, 500, NavMesh.AllAreas))
            {

                _transformReferenceOfNavMeshAgent.position = closestHit.position;
                ReferenceOfNavMeshAgent.enabled = true;
            }
        }

        public void ChangeWaypoints(Transform[] waypoints, UnityAction OnReachingDestination = null, bool snapToInitialWaypoint = false)
        {

            _currentIndexOfWaypoint = 0;
            _numberOfWaypoints = waypoints.Length;
            _waypoints = waypoints;
            _OnReachingDestination = OnReachingDestination;

            CalculateValidDistance();

            if (snapToInitialWaypoint)
                _transformReferenceOfNavMeshAgent.position = _waypoints[_currentIndexOfWaypoint].position;

            ReferenceOfNavMeshAgent.SetDestination(_waypoints[_currentIndexOfWaypoint].position);
        }

        public async void MovementController(Transform[] waypoints, UnityAction<NavMeshAgentMovementController, UnityAction> OnTaskEnd, UnityAction OnReachingDestination = null, bool snapToInitialWaypoint = false)
        {

            WaitForSeconds cycleDelay = new WaitForSeconds(0.33f);

            IsControllerRunning = true;
            _OnReachingDestination = OnReachingDestination;

            ChangeWaypoints(waypoints, OnReachingDestination, snapToInitialWaypoint);

            while (!_isForceQuit)
            {

                if (ReferenceOfNavMeshAgent == null)
                {
                    _canceletionToken.Cancel();
                    break;
                }

                if (Vector3.Distance(_transformReferenceOfNavMeshAgent.position, _waypoints[_currentIndexOfWaypoint].position) <= _validDistanceFromDestination)
                {
                    if (AssignNewWaypointIfValid((1 + _currentIndexOfWaypoint)))
                        UpdateNewDestination(_currentIndexOfWaypoint);
                    else
                        break;

                }

                await Task.Delay(33);
            }

            if (!_canceletionToken.IsCancellationRequested)
                _canceletionToken.Cancel();

            OnTaskEnd.Invoke(this, _OnReachingDestination);

            IsControllerRunning = false;
        }

        public void Stop()
        {

            _isForceQuit = true;
        }

        #endregion

    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    public List<NavMeshAgentMovementController> ListOfActiveNavMeshAgent
    {
        get { return _listOfNavMeshAgentMovementController; }
    }

#endif

#endregion

    #region Private Varaibles

    private int numberOfNavMeshAgentMovementController = 0;
    private List<NavMeshAgentMovementController> _listOfNavMeshAgentMovementController;

    #endregion

    #region OnScriptableObjectCallback

    public void OnEnable()
    {
        _listOfNavMeshAgentMovementController = new List<NavMeshAgentMovementController>();
    }

    public void OnDisable()
    {
        numberOfNavMeshAgentMovementController = 0;
        _listOfNavMeshAgentMovementController.Clear();
    }

    public void OnDestroy()
    {
        numberOfNavMeshAgentMovementController = 0;
        _listOfNavMeshAgentMovementController.Clear();
    }

    #endregion

    #region Configuretion

    private int IsNavMeshAgentMovementControllerAlreadyCreated(NavMeshAgent navMeshAgent)
    {

        for (int i = 0; i < numberOfNavMeshAgentMovementController; i++)
        {
            if (_listOfNavMeshAgentMovementController[i].ReferenceOfNavMeshAgent == navMeshAgent)
            {
                return i;
            }
        }

        return -1;
    }

    private void StopCoroutineOfNavMeshAgentMovementController(NavMeshAgentMovementController navMeshAgentMovementController, UnityAction OnReachingDestination = null)
    {
        _listOfNavMeshAgentMovementController.Remove(navMeshAgentMovementController);
        numberOfNavMeshAgentMovementController--;
        navMeshAgentMovementController = null;

        OnReachingDestination?.Invoke();
    }

    #endregion

    #region Public Callback

    public void MoveAgent(NavMeshAgent navMeshAgent, Transform[] waypoints, UnityAction OnReachingDestination = null, bool snapToInitialPoint = false)
    {


        int index = IsNavMeshAgentMovementControllerAlreadyCreated(navMeshAgent);
        
        if (index == -1)
        {
            _listOfNavMeshAgentMovementController.Add(new NavMeshAgentMovementController(navMeshAgent));
            _listOfNavMeshAgentMovementController[numberOfNavMeshAgentMovementController++].MovementController(
                waypoints,
                StopCoroutineOfNavMeshAgentMovementController,
                OnReachingDestination,
                snapToInitialPoint);

            
        }
        else
        {
            if (_listOfNavMeshAgentMovementController[index].IsControllerRunning) _listOfNavMeshAgentMovementController[index].ChangeWaypoints(waypoints, OnReachingDestination, snapToInitialPoint);
        }

    }

    #endregion


}
