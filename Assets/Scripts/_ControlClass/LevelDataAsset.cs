﻿using UnityEngine;
using UnityEngine.Events;
using com.faith.core;
using com.faith.coreconsole;

[CreateAssetMenu(fileName = "LevelDataAsset", menuName = GlobalConstant.GAME_NAME + "/GameFlow/LevelDataAsset")]
public class LevelDataAsset : ScriptableObject
{

    #region Custom Variables

    [System.Serializable]
    private class WaveInformation
    {
        #region Serialized Field

        public IntReference             numberOfEnemyOnThisWave;
        public RangeReference           intervalOnSpawningEnemy;
        public AITypeContainerAsset     aiTypesOnThisWave;

        #endregion
    }

    [System.Serializable]
    public class LevelInformation
    {
        #region Serialized Field

        [SerializeField] private SceneReference sceneReference;
        [SerializeField] private WaveInformation[] waveInformations;

        #endregion

        #region Configuretion

        private bool IsValidWaveIndex(int waveIndex) {

            if (waveIndex >= 0 && waveIndex < NumberOfWave)
                return true;

            CoreConsole.LogError(string.Format("Invalid WaveIndex = {0}", waveIndex), ConfiguretionFileID.DefaultSettings);
            return false;
        }

        #endregion

        #region Public Callback

        public SceneReference SceneReference { get { return sceneReference; } }
        public int NumberOfWave { get { return waveInformations.Length; } }

        public int GetNumberOfEnemyOnWave(int waveIndex) {

            if (IsValidWaveIndex(waveIndex)) {

                return waveInformations[waveIndex].numberOfEnemyOnThisWave;
            }

            return -1;
        }

        public RangeReference GetAISpawnInterval(int waveIndex)
        {

            if (IsValidWaveIndex(waveIndex))
            {
                return waveInformations[waveIndex].intervalOnSpawningEnemy;
            }

            return null;
        }

        public AITypeContainerAsset GetAITypeContainer(int waveIndex) {

            if (IsValidWaveIndex(waveIndex))
            {
                return waveInformations[waveIndex].aiTypesOnThisWave;
            }

            return null;
        }

        #endregion
    }

    #endregion

    #region Public Variables

    public int GetLevelIndex { get { return _levelIndex.GetData(); } }

    [Range(1, 1000)]
    public int maxLevel = 1;

    [Header("Material   :   WorldBending Material")]
    public float effectiveDistance;
    public Material[] worldBendingMaterials;
    public Material[] inverseWorldBendingMaterials;

    [Header("Data   :   Shared")]
    public SharedData sharedGameData;

    [Header("Data   :   LevelData")]
    public LevelInformation[] levelInformations;

    #endregion

    #region Private Varaibles

    private bool            _isDataLoaded;
    private SavedData<int>  _levelIndex;
    private UnityAction     OnLevelDataLoaded;

    #endregion

    #region Configuretion

    private void OnLevelIndexLoaded(int _loadedDayIndex)
    {

        sharedGameData.levelIndex = _loadedDayIndex;
        if (!_isDataLoaded)
        {
            _isDataLoaded = true;
            OnLevelDataLoaded.Invoke();
        }
    }

    private bool IsValidLevelIndex(int dayIndex)
    {

        if (dayIndex >= 0 && dayIndex < maxLevel)
        {

            return true;
        }

        return false;
    }

    private void SetEffectiveDistance()
    {
        foreach (Material worldBendingMaterial in worldBendingMaterials)
        {

            worldBendingMaterial.SetFloat("_EffectiveDistance", effectiveDistance);
        }

        foreach (Material inverseWorldBendingMaterial in inverseWorldBendingMaterials)
        {

            inverseWorldBendingMaterial.SetFloat("_EffectiveDistance", effectiveDistance);
        }
    }

    #endregion

    #region Public Callback

    public void Initialization(UnityAction OnLevelDataLoaded)
    {
        SetValueForWorldBendingMaterial(Vector2.zero);

        _isDataLoaded = false;
        this.OnLevelDataLoaded = OnLevelDataLoaded;

        sharedGameData = new SharedData();
        _levelIndex = new SavedData<int>("LEVEL_INDEX", 0, OnLevelIndexLoaded);

        SetEffectiveDistance();

    }

    public LevelInformation GetLevelInformationForCurrentLevel() {

        return levelInformations[_levelIndex.GetData()];
    }

    

    public void SetValueForWorldBendingMaterial(Vector2 bendValue) {

        foreach (Material worldBendingMaterial in worldBendingMaterials) {

            worldBendingMaterial.SetFloat("HorizontalDisplacement", bendValue.x);
            worldBendingMaterial.SetFloat("VerticalDisplacement", bendValue.y);
        }

        foreach (Material inverseWorldBendingMaterial in inverseWorldBendingMaterials) {

            inverseWorldBendingMaterial.SetFloat("HorizontalDisplacement", -bendValue.x);
            inverseWorldBendingMaterial.SetFloat("VerticalDisplacement", -bendValue.y);
        }
    }

    public void GotToNextLevel()
    {

        int nextDay = _levelIndex.GetData() + 1;
        if (IsValidLevelIndex(nextDay))
            _levelIndex.SetData(nextDay);
        else
        {
            _levelIndex.SetData(0);
        }
    }


    #endregion
}
