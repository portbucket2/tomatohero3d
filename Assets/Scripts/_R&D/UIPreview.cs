﻿using UnityEngine;
using UnityEngine.UI;

public class UIPreview : MonoBehaviour
{
    public RawImage rawImageReference;
    public BaseClassForDecaleObjectUsingTexcoord decaleObjectReference;

    
    // Update is called once per frame
    void Update()
    {
        rawImageReference.texture = decaleObjectReference.DecaleTexture();
    }
}
