﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using com.faith.core;
using com.faith.gameplay.service;

[RequireComponent(typeof(CharacterController))]
public class InputSystemForPlayer : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        _characterController = gameObject.GetComponent<CharacterController>();

        _initialLocalEulerAngle = cameraTransformReference.localEulerAngles;
        
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnLevelStarted()
    {
        base.OnLevelStarted();

        _characterController.OnDamageTaken += OnCameraShake;

        GlobalTouchController.Instance.EnableTouchController();

        GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
        GlobalTouchController.Instance.OnTouch += OnTouch;
        GlobalTouchController.Instance.OnTouchUp += OnTouchUp;

    }

    public override void OnLevelEnded()
    {
        base.OnLevelEnded();

        GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
        GlobalTouchController.Instance.OnTouch -= OnTouch;
        GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;

        GlobalTouchController.Instance.DisableTouchController();

        _characterController.OnDamageTaken -= OnCameraShake;

    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables



    [Header("External Reference :   Camera")]
    public Transform cameraTransformReference;

    [Space(2.5f)]
    public Joystick joystickReference;

    
    [Space(5.0f)]
    [Range(0f,3f)]
    public float initialDelayOnShooting = 0;


    [Space(5.0f)]
    public bool invertVerticalControl;
    [Range(1, 100)]
    public float validTouchDistance = 1f;
    [Range(0.01f,0.2f)]
    public float lookAtVelocity = 0.1f;
    public RangeReference horizontalLookingArea;
    public RangeReference verticalLookingArea;

    public CharacterController CharacterControllerReference { get { return _characterController; } }

    #endregion

    #region Private Variables

    private CharacterController _characterController;

    private bool    _isTouchActive = false;
    private bool _isShootingActive = false;

    private float remainingTimeOnDelayOnShooting;

    private Vector3 _initialLocalEulerAngle;
    private Vector3 _touchDownPosition;


    private Vector2 _joystickInput = new Vector2(0.5f, 0.5f);

    private UnityAction         OnReadyToShooting;
    private UnityAction<float>  OnPrepearingToShoot;

    #endregion

    #region Mono Behaviour

#if UNITY_EDITOR

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {

            OnShootingStart();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            OnShooting();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            OnShootingStop();
        }
    }

#endif

    #endregion

    #region Configuretion

    private void OnCameraShake(float value) {

        //cameraShakeReference.ShowCameraShake();
    }

    private IEnumerator InitialDelayOnFire() {

        remainingTimeOnDelayOnShooting = initialDelayOnShooting;
        float cycleLength = 0.0167f;
        WaitForSeconds cycleDelay = new WaitForSeconds(cycleLength);
        

        while (remainingTimeOnDelayOnShooting > 0) {

            remainingTimeOnDelayOnShooting -= cycleLength;

            OnPrepearingToShoot?.Invoke(1f - (remainingTimeOnDelayOnShooting / initialDelayOnShooting));

            yield return cycleDelay;
        }

        OnPrepearingToShoot?.Invoke(1);
        OnReadyToShooting?.Invoke();

        StopCoroutine(InitialDelayOnFire());

        _isShootingActive = true;
    }

    private void OnShootingStart() {

        _isTouchActive = true;

        StartCoroutine(InitialDelayOnFire());
    }

    private void OnShooting() {

        if (_isShootingActive)
        {
            _characterController.selectedWeapon.Shoot(
                cameraTransformReference.forward,
                _characterController.selectedWeapon.weaponAttribute.bulletParameters.ignoringObjectsWithTags);

            
        }

    }

    private void OnShootingStop() {

        remainingTimeOnDelayOnShooting = 0;

        _isShootingActive = false;
        _isTouchActive = false;
    }

    private void OnTouchDown(Vector3 touchPosition, int touchIndex) {

        _touchDownPosition = touchPosition;

        OnShootingStart();
    }

    private void OnTouch(Vector3 touchPosition, int touchIndex) {

        if (_isTouchActive)
        {
            _joystickInput = new Vector2(
                (joystickReference.Horizontal + 1) / 2.0f,
                (joystickReference.Vertical + 1) / 2.0f);

            
            Vector3 lookDirection = new Vector3(
                invertVerticalControl ? verticalLookingArea.InterpolatedValue(_joystickInput.y) : verticalLookingArea.InterpolatedValue(1f - _joystickInput.y),
                horizontalLookingArea.InterpolatedValue(_joystickInput.x),
                0
            );

            Quaternion modifiedRotation = Quaternion.Slerp(
                        cameraTransformReference.localRotation,
                        Quaternion.Euler(_initialLocalEulerAngle + lookDirection),
                        lookAtVelocity
                    );

            cameraTransformReference.localRotation = modifiedRotation;

            _touchDownPosition = touchPosition;
        }
        else {

            if (Vector3.Distance(_touchDownPosition, touchPosition) >= validTouchDistance)
            {
                _isTouchActive = true;
            }
        }

        OnShooting();

    }

    private void OnTouchUp(Vector3 touchPosition, int touchIndex)
    {
        OnShootingStop();
    }

    #endregion

    #region Public Callback

    public void PreProcess(UnityAction OnReadyToShooting, UnityAction<float> OnPrepearingToShoot = null) {

        this.OnReadyToShooting = OnReadyToShooting;
        this.OnPrepearingToShoot = OnPrepearingToShoot;
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
