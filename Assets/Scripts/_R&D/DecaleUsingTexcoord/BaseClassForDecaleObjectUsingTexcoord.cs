﻿using UnityEngine;

public abstract class BaseClassForDecaleObjectUsingTexcoord : TomatoHero3DBehaviour, IDecale
{
    #region Private Variables

    private Material _material;
    private Texture2D _copiedVersionOfMainTexture;

    private Color[]     _originalColorBuffer;
    private Color[]     _currentColorBuffer;
    private int         _textureWidth;
    private int         _textureHeight;

    #endregion

    #region Configuretion

    private bool IsValidPixel(int x, int y)
    {
        if (x >= 0 && x < _textureHeight && y >= 0 && y < _textureHeight)
            return true;

        return false;
    }

    private Texture2D DecaleMesh(
        Vector2 textureCoordPosition,
        ref Color[] colorBufferForDecaleTexture,
        ref Vector2Int sizeOfColorBufferForDecaleTexture,
        float spreadAmount,
        float rateOfChange)
    {

        Vector2Int centerPointOfDecaleTexture = new Vector2Int(
                (int)(textureCoordPosition.x * _textureWidth),
                (int)(textureCoordPosition.y * _textureHeight)
            );

        Debug.Log(string.Format("TexCord = {0} : CenterPoint = {1}", textureCoordPosition, centerPointOfDecaleTexture));

        Vector2Int startPoint = new Vector2Int(
                (int)(centerPointOfDecaleTexture.x - (_textureWidth * spreadAmount)),
                (int)(centerPointOfDecaleTexture.y - (_textureHeight * spreadAmount))
            );
        Vector2Int endPoint = new Vector2Int(
                (int)(centerPointOfDecaleTexture.x + (_textureWidth * spreadAmount)),
                (int)(centerPointOfDecaleTexture.y + (_textureHeight * spreadAmount))
            );

        float widthForDecaleTextureOverTheSpreading = endPoint.x - startPoint.x;
        float heightForDecaleTextureOverTheSpreading = endPoint.y - startPoint.y;

        Debug.Log(string.Format("StartPoint = {0} : EndPoint = {1} : Size = ({2},{3})", startPoint, endPoint, widthForDecaleTextureOverTheSpreading, heightForDecaleTextureOverTheSpreading));

        for (int x = startPoint.x; x < endPoint.x; x++)
        {

            for (int y = startPoint.y; y < endPoint.y; y++)
            {

                if (IsValidPixel(x, y))
                {

                    int indexAtColorBuffer = (x * _textureWidth) + y;

                    Vector2Int positionAtDecaleTexture = new Vector2Int(
                            (int)(((x - startPoint.x) / widthForDecaleTextureOverTheSpreading) * sizeOfColorBufferForDecaleTexture.x),
                            (int)(((y - startPoint.y) / heightForDecaleTextureOverTheSpreading) * sizeOfColorBufferForDecaleTexture.y)
                        );
                    int indexAtColorBufferOnDecaleTexture = (positionAtDecaleTexture.x * sizeOfColorBufferForDecaleTexture.x) + positionAtDecaleTexture.y;

                    //Debug.Log(string.Format("{0} <-> {1}",
                    //    ((x - startPoint.x) / widthForDecaleTextureOverTheSpreading) * sizeOfColorBufferForDecaleTexture.x,
                    //    ((y - startPoint.y) / heightForDecaleTextureOverTheSpreading) * sizeOfColorBufferForDecaleTexture.y));
                    //Debug.Log(string.Format("(x,y) = ({0},{1}), IndexAtColorBuffer = {2}, IndexAtColorBufferOfDecale = {3}", x, y, indexAtColorBuffer, indexAtColorBufferOnDecaleTexture));

                    Color currentPixelColor = _currentColorBuffer[indexAtColorBuffer];
                    Color targetedPixelColor = colorBufferForDecaleTexture[indexAtColorBufferOnDecaleTexture];
                    Color newColor = new Color(
                            ((1 - targetedPixelColor.a) * currentPixelColor.r) + (targetedPixelColor.a * targetedPixelColor.r),
                            ((1 - targetedPixelColor.a) * currentPixelColor.g) + (targetedPixelColor.a * targetedPixelColor.g),
                            ((1 - targetedPixelColor.a) * currentPixelColor.b) + (targetedPixelColor.a * targetedPixelColor.b),
                            1
                        );

                    _currentColorBuffer[indexAtColorBuffer] = Color.Lerp(
                            _currentColorBuffer[indexAtColorBuffer],
                            newColor,
                            rateOfChange
                        );
                }
            }
        }

        _copiedVersionOfMainTexture.SetPixels(_currentColorBuffer);
        _copiedVersionOfMainTexture.Apply();

        return _copiedVersionOfMainTexture;

    }


    #endregion

    #region Protected Method

    /// <summary>
    /// Pleae pass the 'Material' and 'MainTexture' reference by calling 'PreProcess' from BaseClass
    /// </summary>
    protected abstract void PassMaterialAndMainTextureReference();

    protected void PreProcess(Material material, Texture mainTexture)
    {
        _material = material;

        _copiedVersionOfMainTexture = new Texture2D(mainTexture.width, mainTexture.height);
        _copiedVersionOfMainTexture.SetPixels(((Texture2D)mainTexture).GetPixels());
        _copiedVersionOfMainTexture.Apply();

        _originalColorBuffer = _copiedVersionOfMainTexture.GetPixels();
        _currentColorBuffer = _originalColorBuffer;
        _textureWidth = _copiedVersionOfMainTexture.width;
        _textureHeight = _copiedVersionOfMainTexture.height;
    }

    #endregion

    #region Public Callback

    public Texture2D DecaleTexture()
    {
        return _copiedVersionOfMainTexture;
    }

    public void Decale(
        Vector2 textureCoordPosition,
        ref Color[] colorBufferForDecaleTexture,
        ref Vector2Int sizeOfColorBufferForDecaleTexture,
        float spreadAmount,
        float rateOfChange)
    {
        _material.mainTexture =  DecaleMesh(
            textureCoordPosition,
            ref colorBufferForDecaleTexture,
            ref sizeOfColorBufferForDecaleTexture,
            spreadAmount,
            rateOfChange);
    }

    #endregion


}
