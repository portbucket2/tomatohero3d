﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecaleCharacterObjectUsingVertex : BaseClassForDecaleObjectUsingVertex
{
    #region Public Varaibles

    public MeshFilter   meshFiltererReference;
    public MeshRenderer meshRenderer;

    public BaseClassForDecaleObjectUsingTexcoord texcoordDecale;

    #endregion

    #region Private Variables

    private Mesh _mesh;
    [SerializeField] private Vector3[]   _vertices;
    [SerializeField] private Vector2[]   _uv;

    [SerializeField] private Texture2D _copiedVersionOfMainTexture;

    private Color[] _originalColorBuffer;
    private Color[] _currentColorBuffer;
    private int _textureWidth;
    private int _textureHeight;

    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        PreProcess();
    }
    #endregion

    #region Configuretion

    private void PreProcess() {


        _mesh           = meshFiltererReference.mesh;
        
        _vertices       = _mesh.vertices;
        _uv             = _mesh.uv;

        _copiedVersionOfMainTexture = new Texture2D(meshRenderer.material.mainTexture.width, meshRenderer.material.mainTexture.height);
        _copiedVersionOfMainTexture.SetPixels(((Texture2D)meshRenderer.material.mainTexture).GetPixels());
        _copiedVersionOfMainTexture.Apply();

        _originalColorBuffer = _copiedVersionOfMainTexture.GetPixels();
        _currentColorBuffer = _originalColorBuffer;
        _textureWidth = _copiedVersionOfMainTexture.width;
        _textureHeight = _copiedVersionOfMainTexture.height;

        Debug.Log(string.Format("vertices = {0}, triangles = {1}, uv = {2}, width = {3}, height = {4}", _vertices.Length, _mesh.triangles.Length, _uv.Length, _textureWidth, _textureHeight));
    }



    #endregion

    #region Public Callback

    public void DecaleTexcoord(
        int triangleIndex,
        ref Color[] colorBufferForDecaleTexture,
        ref Vector2Int sizeOfColorBufferForDecaleTexture,
        float spreadAmount,
        float rateOfChange)
    {
        Vector2 texcoord = Vector2.zero;
        for (int i = 0; i < 3; i++)
        {
            int uvIndex = _mesh.triangles[(3 * triangleIndex) + i];
            texcoord += _uv[uvIndex];
            Debug.Log(string.Format("UV Index = {0}, Texcoord = {1}", uvIndex, _uv[uvIndex]));
        }
        texcoord /= 3;

        texcoordDecale.Decale(
                texcoord,
                ref colorBufferForDecaleTexture,
                ref sizeOfColorBufferForDecaleTexture,
                spreadAmount,
                rateOfChange
            );
    }

    public void Decale(
        int triangleIndex,
        ref Color[] colorBufferForDecaleTexture,
        ref Vector2Int sizeOfColorBufferForDecaleTexture,
        float spreadAmount,
        float rateOfChange) {

        Debug.Log(triangleIndex);

        Vector3 absoluteHitPointOverVertex = Vector3.zero;
        Vector3[] triangleVertices = new Vector3[3];
        for (int i = 0; i < 3; i++) {

            triangleVertices[i] = _vertices[_mesh.triangles[(3 * triangleIndex) + i]];
            absoluteHitPointOverVertex += triangleVertices[i];
        }

        absoluteHitPointOverVertex /= 3;
        Vector3 interpolatedSize    = _mesh.bounds.size * spreadAmount;
        float validDistance         = Vector3.Distance(absoluteHitPointOverVertex, absoluteHitPointOverVertex + interpolatedSize);

        //Finding The Nearest PointSssss on Hit
        List<int> listOfNearestVerticesIndex    = new List<int>();
        List<float> listOfDistances             = new List<float>();
        for (int i = 0; i < _mesh.vertexCount; i++)
        {
            float currentDistance = Vector3.Distance(absoluteHitPointOverVertex, _vertices[i]);
            if (currentDistance <= validDistance)
            {
                listOfNearestVerticesIndex.Add(i);
                listOfDistances.Add(currentDistance);
            }
        }

        int numberOfNearestVertices = listOfNearestVerticesIndex.Count;
        
        Debug.Log(string.Format("HitPoint = {0}, SizeBaseOnSpreadAmount = {1}, ValidDistance = {2}, NumberOfVerticesNearby = {3}" ,absoluteHitPointOverVertex, interpolatedSize, validDistance, numberOfNearestVertices));

        for (int i = 0; i < numberOfNearestVertices; i++)
        {

            Vector2 texcoord = _uv[listOfNearestVerticesIndex[i]];
            int pixelIndexOnMainTexture = (int)((texcoord.x * (Mathf.Pow(_textureWidth, 2) - _textureWidth)) + (texcoord.y * _textureHeight));


            float weight = listOfDistances[i] / validDistance;
            Vector3 unitVector = Vector3.Normalize(_vertices[listOfNearestVerticesIndex[i]] - absoluteHitPointOverVertex);

            Vector2 decaleTexcoord = new Vector2(
                    ((unitVector.x + 1.0f) / 2.0f) * weight,
                    ((unitVector.z + 1.0f) / 2.0f) * weight
                );
            int pixelIndexOnDecaleTexture   =  (int) ((decaleTexcoord.x *  (Mathf.Pow(sizeOfColorBufferForDecaleTexture.x,2) - sizeOfColorBufferForDecaleTexture.x)) + (decaleTexcoord.y * sizeOfColorBufferForDecaleTexture.y));

            Debug.Log(string.Format(
                "Texcoord = {0} : PixelIndexOnMainTex = {1}, DecaleTexcoord = {2} : PixelIndexOnDecale = {3}",
                texcoord,
                pixelIndexOnMainTexture,
                decaleTexcoord,
                pixelIndexOnDecaleTexture));

            Color currentPixelColor = _currentColorBuffer[pixelIndexOnMainTexture];
            Color targetedPixelColor = colorBufferForDecaleTexture[pixelIndexOnDecaleTexture];
            Color newColor = new Color(
                    ((1 - targetedPixelColor.a) * currentPixelColor.r) + (targetedPixelColor.a * targetedPixelColor.r),
                    ((1 - targetedPixelColor.a) * currentPixelColor.g) + (targetedPixelColor.a * targetedPixelColor.g),
                    ((1 - targetedPixelColor.a) * currentPixelColor.b) + (targetedPixelColor.a * targetedPixelColor.b),
                    1
                );

            _currentColorBuffer[pixelIndexOnMainTexture] = Color.Lerp(
                    currentPixelColor,
                    targetedPixelColor,
                    1
                );

            //Debug.Log(string.Format("PixelIndexOnDecale {0}, Color = {1}", pixelIndexOnDecaleTexture, targetedPixelColor));
            //Debug.Log(string.Format("PixelIndexOnMainTex = {0}, PixelIndexOnDecale = {1}, CurrentColor = {2}, TargetedColor = {3}", pixelIndexOnMainTexture, pixelIndexOnDecaleTexture, currentPixelColor, targetedPixelColor));
            //Debug.Log(string.Format("VertexIndex = {0}, VertexPosition = {1}, InterpolatedValue = {2}", listOfNearestVerticesIndex[i], _vertices[listOfNearestVerticesIndex[i]], decaleTexcoord));
        }

        _copiedVersionOfMainTexture.SetPixels(_currentColorBuffer);
        _copiedVersionOfMainTexture.Apply();

        meshRenderer.material.mainTexture = _copiedVersionOfMainTexture;
    }

    public void Decale(
        Vector3 hitPosition,
        ref Color[] colorBufferForDecaleTexture,
        ref Vector2Int sizeOfColorBufferForDecaleTexture,
        float spreadAmount,
        float rateOfChange)
    {

        //Finding The Nearest Point on Hit
        float   smallestDistance            = Mathf.Infinity;
        int     nearestVertexIndexOnHit     = 0;
        Vector3 nearestVertexPositioOnHit   = Vector3.zero;

        for (int i = 0; i < _mesh.vertexCount; i++) {

            float currentDistance = Vector3.Distance(hitPosition, _vertices[i]);
            if (currentDistance < smallestDistance) {

                smallestDistance = currentDistance;

                nearestVertexIndexOnHit = i;
                nearestVertexPositioOnHit = _vertices[i];
            }
        }

        //Finding The Nearest PointSssss on Hit
        List<int> listOfNearestVertices = new List<int>();
        for (int i = 0; i < _mesh.vertexCount; i++) {

            if (Vector3.Distance(nearestVertexPositioOnHit, _vertices[i]) <= spreadAmount) {

                listOfNearestVertices.Add(i);
            }
        }


        int numberOfNearestVertices = listOfNearestVertices.Count;
        Vector3 interpolatedSize    = _mesh.bounds.size * spreadAmount;
        Debug.Log("InterpolatedSiz = " + interpolatedSize);
        for (int i = 0; i < numberOfNearestVertices; i++) {

            Vector2 interpolatedValue = new Vector2(
                    ((_vertices[listOfNearestVertices[i]].x / interpolatedSize.x) + 1) / (interpolatedSize.x * 2),
                    ((_vertices[listOfNearestVertices[i]].z / interpolatedSize.z) + 1) / (interpolatedSize.z * 2)
                );

            Debug.Log(string.Format("VertexIndex = {0}, VertexPosition = {1}, InterpolatedValue = {2}", listOfNearestVertices[i], _vertices[listOfNearestVertices[i]], interpolatedValue));
        }
    }

    #endregion
}
