﻿using UnityEngine;
using com.faith.core;

[RequireComponent(typeof(CharacterController))]
public class WeaponLoadoutForPlayer : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        _characterController = gameObject.GetComponent<CharacterController>();
        _selectedWeaponIndex = new SavedData<int>("PLAYER_SELECTED_WEAPON_INDEX", 0, (weaponIndex)=> {

            if (IsValidIndex(weaponIndex)) {

                int numberOfAvailableWeapon = availableWeapon.Length;
                for (int i = 0; i < numberOfAvailableWeapon; i++) {

                    if (availableWeapon[i].fbxContainer != null) {

                        availableWeapon[i].fbxContainer.SetActive(i == weaponIndex);
                    }
                }
                _characterController.selectedWeapon = availableWeapon[weaponIndex];
            }
        });
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variales

    public WeaponController[]   availableWeapon;
    public int SelectedWeaponIndex { get { return _selectedWeaponIndex.GetData(); } }

    #endregion

    #region Private Variables

    private CharacterController _characterController;
    private SavedData<int>      _selectedWeaponIndex;

    #endregion

    #region Configuretion

    private bool IsValidIndex(int index) {

        if (index >= 0 && index < availableWeapon.Length)
            return true;

        Debug.LogError("Invalid WeaponIndex = " + index);

        return false;
    }

    #endregion


    #region Public Callback

    public void SelectWeapon(int weaponIndex) {

        _selectedWeaponIndex.SetData(weaponIndex);
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
