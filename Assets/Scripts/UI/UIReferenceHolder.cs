﻿using UnityEngine;

public class UIReferenceHolder : MonoBehaviour
{
    public UIMainMenu UIMainMenuReference;
    public UIGameplayMenu UIGameplayMenuReference;

}
