﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.faith.core;
using com.faith.coreconsole;

public class UIMainMenu : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        buttonForStart.onClick.AddListener(() =>
        {
            rootContainer.SetActive(false);
            gameManager.ChangeGameState(GameState.LevelStarted);
        });
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDataLoaded()
    {
        base.OnDataLoaded();

        rootContainer.SetActive(true);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public GameObject   rootContainer;

    [Space(5.0f)]
    public Button       buttonForStart;

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
