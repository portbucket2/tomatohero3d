﻿using UnityEngine;
using com.faith.core;

public class DecaleGun : MonoBehaviour
{
    #region Public Variables

    [Header("Reference  :   External")]
    public Camera       cameraReference;
    public Texture2D    decaleTexture;

    [Header("Parameter")]
    public Color colorForDecale;
    public RangeReference spreadingAmount;
    public FloatReference rateOfChangeOnColor;

    #endregion

    #region Private Variables

    private Color[] colorBufferForDecaleTexture;
    private Vector2Int sizeofColorBufferForDecaleTexture;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        colorBufferForDecaleTexture         = decaleTexture.GetPixels();

        int arraySize = colorBufferForDecaleTexture.Length;
        for (int i = 0; i < arraySize; i++) {

            float alpha = colorBufferForDecaleTexture[i].a;
            colorBufferForDecaleTexture[i] = colorForDecale;
            colorBufferForDecaleTexture[i].a = alpha;
        }

        sizeofColorBufferForDecaleTexture   = new Vector2Int(decaleTexture.width, decaleTexture.height);

        Debug.Log(string.Format("DecaleTexture : SizeOfTexture ({0},{1}), SizeOfColorBuffer = {2}", decaleTexture.width, decaleTexture.height, sizeofColorBufferForDecaleTexture));
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) {

            RaycastHit raycastHit;
            if (Physics.Raycast(cameraReference.ScreenPointToRay(Input.mousePosition), out raycastHit)){

                IDecale decaleObjectReference = raycastHit.collider.GetComponent<IDecale>();
                if (decaleObjectReference != null) {

                    Debug.Log("Texcoord = " + raycastHit.textureCoord);

                    decaleObjectReference.Decale(
                        raycastHit.textureCoord,
                        ref colorBufferForDecaleTexture,
                        ref sizeofColorBufferForDecaleTexture,
                        spreadingAmount.Value,
                        rateOfChangeOnColor.Value);
                }
            }
        }
    }

    #endregion
}
