﻿using UnityEngine;
using com.faith.core;

[CreateAssetMenu(fileName = "WeaponAttribute", menuName = GlobalConstant.GAME_NAME + "/WeaponSystem/WeaponAttribute", order = 20)]
public class WeaponAttributeAsset : ScriptableObject
{
    #region Public Variables
    [Header("Reference  :   ExternalAsset")]
    public string weaponName;
    public Sprite weaponIcon;



    [Header("Parameter  :   WeaponBehaviour")]
    [Space(2.5f)]
    [Range(0.05f, 1f)]
    public float rateOfFire = 0.05f;
    [Space(2.5f)]
    [Range(0f, 5f)]
    public float durationForReloading = 0.0167f;
    public GameObject muzzleFlash;

    [Header("Parameter  :   Ammunition")]
    public bool isInfiniteAmmo;
    [Range(1,200)]
    public int clipSize = 1;
    [Range(1,600)]
    public int maxAmmoSize = 1;
    public BulletParameterAsset bulletParameters;

    #endregion

    #region Public Callback

    public float RateOfFire { get { return (1 / (60 * rateOfFire)); } }

    #endregion
}
