﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using com.faith.core;

#if UNITY_EDITOR

using UnityEditor;

[CustomEditor(typeof(AIController))]
public class AIControllerEditor : BaseEditorClass
{
    #region Private Variables

    private AIController _reference;

    private SerializedProperty _ragdollRigidbodyReferences;
    private SerializedProperty _ragdollColliderReferences;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        base.OnEnable();

        _reference = (AIController)target;

        if (_reference == null)
            return;

        _ragdollRigidbodyReferences = serializedObject.FindProperty("_ragdollRigidbodyReferences");
        _ragdollColliderReferences = serializedObject.FindProperty("_ragdollColliderReferences");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginHorizontal(GUI.skin.box);
        {
            if (_ragdollRigidbodyReferences.arraySize > 0 || _ragdollColliderReferences.arraySize > 0)
            {

                if (GUILayout.Button("Reset"))
                {

                    _ragdollRigidbodyReferences.arraySize = 0;
                    _ragdollRigidbodyReferences.serializedObject.ApplyModifiedProperties();

                    _ragdollColliderReferences.arraySize = 0;
                    _ragdollColliderReferences.serializedObject.ApplyModifiedProperties();
                }
            }
            else {

                EditorGUILayout.HelpBox("Ragdoll data need to be fetched before using it", MessageType.Warning);

                if (GUILayout.Button("Fetch Ragdoll Reference"))
                {

                    //Fetch :   Rigidbody
                    Rigidbody[] rigidbodyReferences = _reference.GetComponentsInChildren<Rigidbody>();

                    int numberOfRigidbody = rigidbodyReferences.Length;

                    _ragdollRigidbodyReferences.arraySize = numberOfRigidbody;
                    _ragdollRigidbodyReferences.serializedObject.ApplyModifiedProperties();

                    for (int i = 0; i < numberOfRigidbody; i++)
                    {
                        rigidbodyReferences[i].isKinematic = true;

                        SerializedProperty ragdollRigidbody = _ragdollRigidbodyReferences.GetArrayElementAtIndex(i);
                        ragdollRigidbody.objectReferenceValue = rigidbodyReferences[i];
                        ragdollRigidbody.serializedObject.ApplyModifiedProperties();
                    }

                    _ragdollRigidbodyReferences.serializedObject.ApplyModifiedProperties();

                    //Fetch :   Collider
                    Collider[] colliderReferences = _reference.GetComponentsInChildren<Collider>();

                    int numberOfCollider = colliderReferences.Length;

                    _ragdollColliderReferences.arraySize = numberOfRigidbody;
                    _ragdollColliderReferences.serializedObject.ApplyModifiedProperties();

                    for (int i = 0; i < numberOfCollider; i++)
                    {

                        colliderReferences[i].enabled = false;

                        SerializedProperty ragdollCollider = _ragdollColliderReferences.GetArrayElementAtIndex(i);
                        ragdollCollider.objectReferenceValue = colliderReferences[i];
                        ragdollCollider.serializedObject.ApplyModifiedProperties();
                    }

                    _ragdollColliderReferences.serializedObject.ApplyModifiedProperties();

                    //Save Asset
                    EditorUtility.SetDirty(_reference);
                    PrefabUtility.RecordPrefabInstancePropertyModifications(_reference);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
            }

            
        }
        EditorGUILayout.EndHorizontal();

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    public void OnSceneGUI()
    {
        if (_reference.characterControllerReference != null) {

            Handles.Label(
            _reference.transform.position + (Vector3.up * 2),
            string.Format("HP : {0}", _reference.characterControllerReference._currentHealth.ToString()),
            EditorStyles.boldLabel);
        }
        
    }

    #endregion

}


#endif

[RequireComponent(typeof(NavMeshAgent))]
public class AIController : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        _transformReference             = transform;

        _navMeshAgentReference = GetComponent<NavMeshAgent>();
        navMeshAttribute.AssignValue(ref _navMeshAgentReference);

        characterControllerReference.OnDied += (characterController) =>
        {
            gameManager.SharedGameData.remainingNumberOfEnemyToDefeateOnCurrentWave--;
            gameManager.UIReferenceHolderReference.UIGameplayMenuReference.UpdatePlayerProgression();
            StartCoroutine(OnCharacterDied());
        };
       
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Varaibles

    [Header("Parameter  :   Editor")]
#if UNITY_EDITOR
    public Transform rootBoneReference;
#endif

    [Header("Parameter  :   Behaviour")]
    public CharacterController characterControllerReference;
    public NavMeshAgentSharedAttributeAsset navMeshAttribute;
    public AITypeAsset                      aiType;

    [Space(5.0f)]
    public Animator animatorReference;

    #endregion

    #region Serialized Field

    
    [SerializeField, HideInInspector] private Rigidbody[] _ragdollRigidbodyReferences;
    [SerializeField, HideInInspector] private Collider[]    _ragdollColliderReferences;

    #endregion

    #region Private Variables

    private Transform           _transformReference;
    private NavMeshAgent        _navMeshAgentReference;
    

    private Vector3 _destination;

    #endregion

    #region Configuretion

    private void EnableRagdoll() {

        animatorReference.enabled = false;

        foreach (Rigidbody rigidbody in _ragdollRigidbodyReferences) {

            rigidbody.isKinematic = false;
        }

        foreach (Collider collider in _ragdollColliderReferences) {

            collider.enabled = true;
        }
    }

    private void DisableRagdoll() {

        foreach (Rigidbody rigidbody in _ragdollRigidbodyReferences)
        {
            rigidbody.isKinematic = true;
        }

        foreach (Collider collider in _ragdollColliderReferences)
        {

            collider.enabled = false;
        }

        animatorReference.enabled = true;
        animatorReference.SetTrigger("Idle");
    }

    private IEnumerator ControllerForCheckingIfDestinationReached() {

        float cycleLength           = 0.1f;
        WaitForSeconds cycleDelay   = new WaitForSeconds(cycleLength);

        while (Vector3.Distance(_destination, _transformReference.position) > 0.1f)
            yield return cycleDelay;

        animatorReference.SetTrigger("Throw");

        WaitForEndOfFrame waitForEndFrame   = new WaitForEndOfFrame();
        Transform playerTransformReference  = gameManager.playTransformReference;
        Quaternion lookRotationTowardsPlayer= _transformReference.rotation;

        while (lookRotationTowardsPlayer != Quaternion.identity) {

            lookRotationTowardsPlayer = Quaternion.LookRotation(playerTransformReference.position - _transformReference.position);
            _transformReference.rotation = lookRotationTowardsPlayer;

            yield return waitForEndFrame;
        }

        StopCoroutine(ControllerForCheckingIfDestinationReached());
    }

    private IEnumerator OnCharacterDied()
    {
        _navMeshAgentReference.isStopped = true;

        OnDeathImpact();

        yield return new WaitForSeconds(5.0f);

        Pool.GlobalPoolManager.Destroy(gameObject);

        DisableRagdoll();

        StopCoroutine(OnCharacterDied());
    }

    #endregion

    #region Public Callback

    public void Initialize(Vector3 destination) {

        characterControllerReference.ResetHealth();
        _destination = destination;

        animatorReference.SetTrigger("Run");

        _navMeshAgentReference.isStopped = false;
        _navMeshAgentReference.SetDestination(destination);

        StartCoroutine(ControllerForCheckingIfDestinationReached());
    }

    public void ThrowTomato() {

        characterControllerReference.selectedWeapon.ShootTracerBullet(
                gameManager.playTransformReference,
                () => {
                    gameManager.characterControllerForPlayer.Damage(characterControllerReference.selectedWeapon.weaponAttribute.bulletParameters.damagePerShoot);
                });
    }

    public void OnDeathImpact() {

        EnableRagdoll();

        Vector3 impactDirection = Vector3.Normalize(_transformReference.position - gameManager.characterControllerForPlayer.transform.position);
        impactDirection.y = Random.Range(0.4f, 0.5f);

        aiType.Behaviour.PhysicsImpact.ExecutePhysicsImpact(ref _ragdollRigidbodyReferences[0], impactDirection);

    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}

