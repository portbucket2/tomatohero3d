﻿using UnityEngine;

public class DecaleCharacterObjectUsingTexcoord : BaseClassForDecaleObjectUsingTexcoord
{
    #region Public Variables

    public SkinnedMeshRenderer skinMeshRendererReference;

    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        PassMaterialAndMainTextureReference();
    }

    protected override void PassMaterialAndMainTextureReference()
    {
        PreProcess(skinMeshRendererReference.material, skinMeshRendererReference.material.mainTexture);
    }

    #endregion

}
