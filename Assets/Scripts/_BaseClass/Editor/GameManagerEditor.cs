﻿using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    #region Private Variables

    private GameManager _reference;

    private SerializedProperty _listOfTomatoHero3DBehaviour;
    private SerializedProperty _includeDisabledObjectForFindingRailShooterBehaviour;

    #endregion

    #region Editor

    public void OnEnable()
    {
        _reference = (GameManager) target;

        if (_reference == null)
            return;

        _listOfTomatoHero3DBehaviour = serializedObject.FindProperty("_listOfTomatoHero3DBehaviour");
        _includeDisabledObjectForFindingRailShooterBehaviour = serializedObject.FindProperty("_includeDisabledObjectForFindingTomatoHero3DBehaviour");
    }

    public override void OnInspectorGUI()
    {

        serializedObject.Update();

        EditorGUILayout.BeginHorizontal(GUI.skin.box);
        {
            _includeDisabledObjectForFindingRailShooterBehaviour.boolValue = EditorGUILayout.Toggle(
                    "Include Disable",
                    _includeDisabledObjectForFindingRailShooterBehaviour.boolValue
                );
            if (GUILayout.Button("Find All", GUILayout.Width(100)))
            {
                FindAllRailShooterBehaviourOnScene();
            }
        }
        EditorGUILayout.EndHorizontal();
        

        CoreEditorModule.DrawHorizontalLine();
        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    #endregion

    #region Configuretion

    private void FindAllRailShooterBehaviourOnScene() {

        TomatoHero3DBehaviour[] railShooterBehaviours = _reference.transform.GetComponentsInChildren<TomatoHero3DBehaviour>(_includeDisabledObjectForFindingRailShooterBehaviour.boolValue);
        int numberOfRailShooterBehaviour = railShooterBehaviours.Length;

        _listOfTomatoHero3DBehaviour.arraySize = numberOfRailShooterBehaviour - 1;
        for (int i = 1; i < numberOfRailShooterBehaviour; i++) {

            _listOfTomatoHero3DBehaviour.GetArrayElementAtIndex(i - 1).objectReferenceValue = railShooterBehaviours[i];
            _listOfTomatoHero3DBehaviour.GetArrayElementAtIndex(i - 1).serializedObject.ApplyModifiedProperties();
        }

        _listOfTomatoHero3DBehaviour.serializedObject.ApplyModifiedProperties();

        EditorUtility.SetDirty(target);
    }

    #endregion
}
