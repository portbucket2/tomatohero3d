﻿using UnityEngine;

public interface IDecale
{
    
    /// <summary>
    /// Return : Decale Version Of The Texture
    /// </summary>
    /// <returns></returns>
    Texture2D DecaleTexture();

    /// <summary>
    /// Call : 'DecaleMesh(args0...argsN) amd assigned the return texture to your 'MeshRenderer.material.mainTexture'
    /// </summary>
    /// <param name="textureCoordPosition"></param>
    /// <param name="colorBufferForDecaleTexture"></param>
    /// <param name="sizeOfColorBufferForDecaleTexture"></param>
    /// <param name="spreadAmount"></param>
    /// <param name="rateOfChange"></param>
    void Decale(
        Vector2 textureCoordPosition,
        ref Color[] colorBufferForDecaleTexture,
        ref Vector2Int sizeOfColorBufferForDecaleTexture,
        float spreadAmount,
        float rateOfChange);
}
