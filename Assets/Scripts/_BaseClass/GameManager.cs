﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faith.core;
using com.faith.gameplay;

[DefaultExecutionOrder(Constant.EXECUTION_ORDER_GAMEMANAGER)]
public class GameManager : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        Initialization();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnLevelStarted()
    {
        base.OnLevelStarted();
    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variable

    public static GameManager Instance;



    public new bool IsInputSystemInterrupted
    {
        get { return BaseInputSystem.IsInputSystemInterrupted; }
    }

    public GameState CurrentGameState
    {
        get { return gameStateController.gameState; }
    }

    public GameState PreviousGameState
    {
        get { return gameStateController.GetPreviousStateFromStack(); }
    }

    public SharedData SharedGameData
    {
        get { return levelData.sharedGameData; }
    }

    [Header("Reference  :   GameFlow")]
    public GameStateControllerAsset gameStateController;
    public LevelDataAsset           levelData;

    [Header("Reference  :   ServiceLocator")]
    public TimeController   timeControllerReference;
    public AISpawner        aiSpawnerReference;
    public UIReferenceHolder UIReferenceHolderReference;

    [Header("Reference  :   Player")]
    public Transform playTransformReference;
    public CharacterController characterControllerForPlayer;
    public WeaponLoadoutForPlayer weaponLoadoutForPlayer;

    [Header("Parameter  :   SceneLoad")]
    public float initialDelayForSceneLoaded;

    [Header("Parameter  :   UIAnimation")]
    public TransformAnimatorAsset centralAnimatorForUIAppear;
    public TransformAnimatorAsset centralAnimatorForUIDisappear;
    public TransformAnimatorAsset centralAnimatorForUIInteract;

    [Header("Parameter  :   Animation")]
    public TransformAnimatorAsset centralAnimatorForAppear;
    public TransformAnimatorAsset centralAnimatorForDisappear;

    [Header("Reference  :   Tomato3DBehaviour")]
#if UNITY_EDITOR
    [SerializeField] private bool _includeDisabledObjectForFindingTomatoHero3DBehaviour;
#endif
    [SerializeField] private List<TomatoHero3DBehaviour> _listOfTomatoHero3DBehaviour;

    #endregion

    #region Private Variables

    private static bool _isInitialDataLoaded = false;

    #endregion

    #region Configuretion

    private void Initialization()
    {
        Instance = this;

        foreach (TomatoHero3DBehaviour tomatoHero3DBehaviour in _listOfTomatoHero3DBehaviour)
        {
            tomatoHero3DBehaviour.Register();
        }

        if (!_isInitialDataLoaded)
        {

            levelData.Initialization(delegate {

                gameStateController.ChangeGameState(GameState.DataLoaded);
                gameStateController.ChangeGameState(GameState.SceneLoaded);
                _isInitialDataLoaded = true;
            });
        }
    }

    #endregion

    #region PublicCallback

    public void ChangeGameState(GameState gameState)
    {
        gameStateController.ChangeGameState(gameState);
    }

    public void LoadScene(
        SceneReference sceneReference,
        UnityAction OnSceneLoaded = null,
        float initialDelayToInvokeOnSceneLoaded = -1)
    {

        initialDelayToInvokeOnSceneLoaded = initialDelayToInvokeOnSceneLoaded == -1 ? initialDelayForSceneLoaded : initialDelayToInvokeOnSceneLoaded;

        sceneReference.LoadScene(
            OnUpdatingProgression: null,
            OnSceneLoaded: () => {
                gameManager.ChangeGameState(GameState.SceneLoaded);
                OnSceneLoaded?.Invoke();
            },
            initalDelayToInvokeOnSceneLoaded: initialDelayToInvokeOnSceneLoaded);


    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS
}
