﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faith.core;
using com.faith.coreconsole;

public class AISpawner : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
        
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnLevelStarted()
    {
        base.OnLevelStarted();

        StartCoroutine(ControllerForWaveManagement());
    }

    public override void OnLevelEnded()
    {
        base.OnLevelEnded();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Varaibles

    public AISpawnCordniates[] aiSpawnCordinates;

    #endregion

    #region Private Variables

    #endregion

    #region Configuretion

    private IEnumerator ControllerForWaveManagement() {

        WaitUntil waitUntilTheWaveIsFinished = new WaitUntil(() =>
        {
            if (gameManager.SharedGameData.remainingNumberOfEnemyToDefeateOnCurrentWave > 0)
                return false;

            return true;
        });

        int levelIndex                  = gameManager.SharedGameData.levelIndex;
        int numberOfAIWave              = aiSpawnCordinates.Length;

        gameManager.SharedGameData.numberOfWaveInThisLevel          = numberOfAIWave;
        gameManager.SharedGameData.remainingNumberOfWaveInThisLevel = numberOfAIWave;

        for (int waveIndex = 0; waveIndex < numberOfAIWave; waveIndex++) {

            int numberOfEnemyToSpawn                = gameManager.levelData.levelInformations[levelIndex].GetNumberOfEnemyOnWave(waveIndex);
            RangeReference intervalOnSpawningEnemy  = gameManager.levelData.levelInformations[levelIndex].GetAISpawnInterval(waveIndex);
            AITypeContainerAsset aiTypesOnThisWave  = gameManager.levelData.levelInformations[levelIndex].GetAITypeContainer(waveIndex);

            gameManager.SharedGameData.numberOfEnemyToDefeateOnCurrentWave          = numberOfEnemyToSpawn;
            gameManager.SharedGameData.remainingNumberOfEnemyToDefeateOnCurrentWave = numberOfEnemyToSpawn;

            int numberOfSpawnPoint      = aiSpawnCordinates[waveIndex].spawnPoints.Length;
            int numberOfDestinationPoint= aiSpawnCordinates[waveIndex].destinationPoints.Length;

            for (int enemyIndex = 0; enemyIndex < numberOfEnemyToSpawn; enemyIndex++) {

                Vector3 destinationPoint = aiSpawnCordinates[waveIndex].destinationPoints[Random.Range(0, numberOfSpawnPoint)].position;
                destinationPoint        += new Vector3(aiSpawnCordinates[waveIndex].offsetAtDestinationPoint, 0, aiSpawnCordinates[waveIndex].offsetAtDestinationPoint);

                AIController aiControllerReference = Pool.GlobalPoolManager.Instantiate(
                    aiTypesOnThisWave.GetRandomAIBlueprint,
                    aiSpawnCordinates[waveIndex].spawnPoints[Random.Range(0, numberOfSpawnPoint)].position,
                    Quaternion.identity
                ).GetComponent<AIController>();

                aiControllerReference.Initialize(destinationPoint);

                yield return new WaitForSeconds(intervalOnSpawningEnemy);
            }

            gameManager.SharedGameData.remainingNumberOfWaveInThisLevel--;

            yield return waitUntilTheWaveIsFinished;
        }

        StopCoroutine(ControllerForWaveManagement());
        gameManager.ChangeGameState(GameState.LevelEnded);
    }

    

    #endregion

    #region Public Callback

    

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
