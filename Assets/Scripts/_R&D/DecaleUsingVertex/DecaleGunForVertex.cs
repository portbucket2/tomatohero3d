﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;

public class DecaleGunForVertex : MonoBehaviour
{
    #region Public Variables

    [Header("Reference  :   External")]
    public Camera cameraReference;
    public Texture2D decaleTexture;

    [Header("Parameter")]
    public Color colorForDecale;
    [Range(0f,1f)]
    public float spreadingAmount = 0.1f;
    public FloatReference rateOfChangeOnColor;

    #endregion

    #region Private Variables

    [SerializeField] private Color[] colorBufferForDecaleTexture;
    private Vector2Int sizeofColorBufferForDecaleTexture;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        colorBufferForDecaleTexture = decaleTexture.GetPixels();

        int arraySize = colorBufferForDecaleTexture.Length;
        for (int i = 0; i < arraySize; i++)
        {

            float alpha = colorBufferForDecaleTexture[i].a;
            colorBufferForDecaleTexture[i] = colorForDecale;
            colorBufferForDecaleTexture[i].a = alpha;
        }

        sizeofColorBufferForDecaleTexture = new Vector2Int(decaleTexture.width, decaleTexture.height);

        Debug.Log(string.Format("DecaleTexture : SizeOfTexture ({0},{1}), SizeOfColorBuffer = {2}", decaleTexture.width, decaleTexture.height, sizeofColorBufferForDecaleTexture));
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            RaycastHit raycastHit;
            if (Physics.Raycast(cameraReference.ScreenPointToRay(Input.mousePosition), out raycastHit))
            {

                DecaleCharacterObjectUsingVertex decaleObjectReference = raycastHit.collider.GetComponent<DecaleCharacterObjectUsingVertex>();
                if (decaleObjectReference != null)
                {
                    decaleObjectReference.DecaleTexcoord(
                            raycastHit.triangleIndex,
                            ref colorBufferForDecaleTexture,
                            ref sizeofColorBufferForDecaleTexture,
                            spreadingAmount,
                            rateOfChangeOnColor
                        );
                    //decaleObjectReference.Decale(
                    //    raycastHit.point,
                    //    ref colorBufferForDecaleTexture,
                    //    ref sizeofColorBufferForDecaleTexture,
                    //    spreadingAmount.Value,
                    //    rateOfChangeOnColor.Value);
                }
            }
        }
    }

    #endregion
}
