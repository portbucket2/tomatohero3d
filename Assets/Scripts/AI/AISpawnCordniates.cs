﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using com.faith.coreconsole;

#if UNITY_EDITOR

using UnityEditor;

[CanEditMultipleObjects]
public class AISpawnCordniatesEditor : BaseEditorClass
{
    #region Private Variables

    private AISpawnCordniates _reference;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        base.OnEnable();

        _reference = (AISpawnCordniates)target;

        if (_reference == null)
            return;
    }


    #endregion
}

#endif

public class AISpawnCordniates : TomatoHero3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("Parameter  :   Editor")]
#if UNITY_EDITOR
    public bool drawGizmos = true;
    public float  scaleForPointIndicator = 1;
#endif

    [Header("Parameter  :   Data & Reference")]
    public Transform[] spawnPoints;
    public Transform[] destinationPoints;
    public RangeReference offsetAtDestinationPoint;

    #endregion

    #region Mono Behaviour

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        if (drawGizmos)
        {

            Color defaultColor = Gizmos.color;

            Gizmos.color = Color.green;
            foreach (Transform spawnPoint in spawnPoints)
            {

                Gizmos.DrawCube(spawnPoint.position, Vector3.one * scaleForPointIndicator);

            }
            Gizmos.color = defaultColor;

            foreach (Transform destinationPoint in destinationPoints)
            {

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(destinationPoint.position, scaleForPointIndicator);

                Gizmos.color = Color.blue;
                Gizmos.DrawWireCube(destinationPoint.position, new Vector3(1, 0.1f, 1) * offsetAtDestinationPoint.Min);

                Gizmos.color = Color.magenta;
                Gizmos.DrawWireCube(destinationPoint.position, new Vector3(1, 0.1f, 1) * offsetAtDestinationPoint.Max);
            }
            Gizmos.color = defaultColor;
        }
    }

#endif

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
