﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using com.faith.core;

public class BulletController : MonoBehaviour
{

    #region Public Variables

    public Rigidbody    rigidbodyReference;
    

    #endregion

    #region Private Variables

    private BulletParameterAsset _bulletParameter;

    private Transform _transformReference;
    private Transform _targetTransformReference;

    private bool    _isHomingBullet;
    private float _damagePerShoot;
    private float   _remainingLifeTime;
    private float   _forwardVelocity;
    private TagReference[] _ignoreCollisionWithTags;

    private UnityAction OnBulletReachedToTarget;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        _transformReference = transform;
        

        enabled = false;
    }

    private void Update()
    {
        if (!_isHomingBullet)
            _remainingLifeTime -= Time.deltaTime;
        else
        {
            Vector3 targetedPosition = _targetTransformReference.position;
            Vector3 modifiedPosition = Vector3.MoveTowards(
                    _transformReference.position,
                    _targetTransformReference.position,
                    _forwardVelocity * Time.deltaTime
                );

            _transformReference.position = modifiedPosition;

            if (Vector3.Distance(modifiedPosition, targetedPosition) <= 0.01f)
            {
                OnBulletReachedToTarget?.Invoke();
                _remainingLifeTime = 0;
            }
        }

        if (_remainingLifeTime <= 0)
            DisposeBullet();
    }


    //private void OnTriggerEnter(Collider other)
    //{
    //    int tagIndex = 0;
    //    foreach (TagReference tagCheck in _ignoreCollisionWithTags)
    //    {
    //        if (string.Equals(other.tag, tagCheck.Value))
    //        {
    //            return;
    //        }

    //        tagIndex++;
    //    }

    //    IDamage IDamgeInterface = other.GetComponent<IDamage>();
    //    if (IDamgeInterface != null)
    //    {
    //        IDamgeInterface.Damage(_damagePerShoot);
    //    }

    //    if (_bulletParameter.isParticleImpactEnabled)
    //    {

    //        Vector3 closestPoint = other.GetComponent<Collider>().ClosestPoint(_transformReference.position);
    //        Quaternion lookRotation = Quaternion.LookRotation(_transformReference.position - closestPoint);
    //        Pool.GlobalPoolManager.Instantiate(
    //                _bulletParameter.GetRandomPrefabReferenceForImpactParticle,
    //                _transformReference.position,
    //                lookRotation
    //            );
    //    }

    //    if (_bulletParameter.isPhysicsImpactEnabled)
    //    {

    //        RaycastHit[] rayCastHitResult = Physics.SphereCastAll(other.GetComponent<Collider>().ClosestPoint(_transformReference.position), _bulletParameter.impactRadius, _transformReference.forward, _bulletParameter.impactRadius, _bulletParameter.impactLayer);
    //        foreach (RaycastHit rayCastHit in rayCastHitResult)
    //        {

    //            FixedJoint[] fixedJoints = rayCastHit.collider.GetComponents<FixedJoint>();
    //            foreach (FixedJoint fixedJoint in fixedJoints)
    //                Destroy(fixedJoint);

    //            Rigidbody rigidbodyReference = rayCastHit.collider.GetComponent<Rigidbody>();
    //            rigidbodyReference.isKinematic = false;
    //            rigidbodyReference.AddForce(-rayCastHit.normal * _bulletParameter.force, _bulletParameter.forceMode);
    //        }
    //    }

    //    DisposeBullet();
    //}

    private void OnCollisionEnter(Collision collision)
    {
        int tagIndex = 0;
        foreach (TagReference tagCheck in _ignoreCollisionWithTags)
        {
            if (string.Equals(collision.collider.tag, tagCheck.Value))
            {
                return;
            }

            tagIndex++;
        }

        IDamage IDamgeInterface = collision.collider.GetComponent<IDamage>();
        if (IDamgeInterface != null)
        {
            IDamgeInterface.Damage(_damagePerShoot);
        }

        if (_bulletParameter.isParticleImpactEnabled)
        {

            Vector3 closestPoint = collision.collider.GetComponent<Collider>().ClosestPoint(_transformReference.position);
            Quaternion lookRotation = Quaternion.LookRotation(_transformReference.position - closestPoint);
            Pool.GlobalPoolManager.Instantiate(
                    _bulletParameter.GetRandomPrefabReferenceForImpactParticle,
                    _transformReference.position,
                    lookRotation
                );
        }

        if (_bulletParameter.isPhysicsImpactEnabled)
        {

            RaycastHit[] rayCastHitResult = Physics.SphereCastAll(collision.collider.GetComponent<Collider>().ClosestPoint(_transformReference.position), _bulletParameter.impactRadius, _transformReference.forward, _bulletParameter.impactRadius, _bulletParameter.impactLayer);
            foreach (RaycastHit rayCastHit in rayCastHitResult)
            {

                FixedJoint[] fixedJoints = rayCastHit.collider.GetComponents<FixedJoint>();
                foreach (FixedJoint fixedJoint in fixedJoints)
                    Destroy(fixedJoint);

                Rigidbody rigidbodyReference = rayCastHit.collider.GetComponent<Rigidbody>();
                rigidbodyReference.isKinematic = false;
                rigidbodyReference.AddForce(-rayCastHit.normal * _bulletParameter.force, _bulletParameter.forceMode);
            }
        }

        StartCoroutine(DisposeBulletWithDelay());
    }


    #endregion

    #region Configuretion

    private void DisposeBullet() {

        rigidbodyReference.velocity = Vector3.zero;
        rigidbodyReference.angularVelocity = Vector3.zero;
        Pool.GlobalPoolManager.Destroy(gameObject);
    }

    private IEnumerator DisposeBulletWithDelay()
    {
        yield return new WaitForSeconds(Time.fixedDeltaTime);

        rigidbodyReference.velocity = Vector3.zero;
        rigidbodyReference.angularVelocity = Vector3.zero;
        Pool.GlobalPoolManager.Destroy(gameObject);
    }

    #endregion

    #region Public Callback

    public void ShootOnDirection(float damagePerShoot, BulletParameterAsset bulletParameter, Vector3 direction, ForceMode forceMode, float forwardVelocity, params TagReference[] ignoreCollisionWithTags) {


        _damagePerShoot = damagePerShoot;
        _bulletParameter = bulletParameter;
        _remainingLifeTime = _bulletParameter.lifeTime;
        _ignoreCollisionWithTags = ignoreCollisionWithTags;

        rigidbodyReference.velocity = Vector3.zero;
        rigidbodyReference.angularVelocity = Vector3.zero;
        _transformReference.rotation = Quaternion.LookRotation(direction);
        

        Vector3 force = direction * forwardVelocity;
        rigidbodyReference.AddForce(force, forceMode);

        _isHomingBullet = false;
        enabled = true;
    }

    public void ShootOnTarget(float damagePerShoot, BulletParameterAsset bulletParameter, Transform targetTransformReference, float forwardVelocity, UnityAction OnBulletReachedToTarget, params TagReference[] ignoreCollisionWithTags) {


        _damagePerShoot = damagePerShoot;
        _bulletParameter = bulletParameter;
        _remainingLifeTime = _bulletParameter.lifeTime;
        _ignoreCollisionWithTags = ignoreCollisionWithTags;


        rigidbodyReference.velocity = Vector3.zero;
        rigidbodyReference.angularVelocity = Vector3.zero;
        _transformReference.rotation = Quaternion.LookRotation(targetTransformReference.position - _transformReference.position);

        _targetTransformReference = targetTransformReference;
        _forwardVelocity = forwardVelocity / 2.0f;
        this.OnBulletReachedToTarget = OnBulletReachedToTarget;

        _isHomingBullet = true;
        enabled = true;
    }

    #endregion
}
