﻿using UnityEngine;

public class DecaleObjectUsingTexcoord : BaseClassForDecaleObjectUsingTexcoord
{
    #region Public Variables

    public MeshRenderer meshRendererReference;

    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        PassMaterialAndMainTextureReference();
    }

    protected override void PassMaterialAndMainTextureReference()
    {
        PreProcess(meshRendererReference.material, meshRendererReference.material.mainTexture);
    }

    #endregion
}
